obj-m        := muennet.o
muennet-objs := debug.o net.o reader.o writer.o

KERNEL_SOURCE ?= /lib/modules/$(shell uname -r)/build

PWD := $(shell pwd)

CFLAGS  ?= $(shell pkg-config --cflags libnl-genl-3.0)
LDFLAGS ?= $(shell pkg-config --libs libnl-genl-3.0)

all: compile-module compile-tool

compile-module:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) modules

muennet_cfg: muennet_cfg.c
	$(CC) -o $@ $< $(CFLAGS) $(LDFLAGS)

compile-tool: muennet_cfg

install: install-module install-tool

install-module: compile-module
	install -d $(DESTDIR)/lib/modules/extra
	install -m 644 muennet.ko $(DESTDIR)/lib/modules/extra

install-tool: compile-tool
	install -d $(DESTDIR)/bin
	install -m 755 muennet_cfg $(DESTDIR)/bin

clean:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) clean
	rm -f muennet_cfg
